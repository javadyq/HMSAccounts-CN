

Pod::Spec.new do |s|
  s.name         = "MyAdditions"
  s.version      = "0.0.2"
  # s.license      = "MIT"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.summary      = "Some category of the framework and UIKit"

  s.homepage     = "https://git.oschina.net/baiyingqiu/MyAdditions"
  s.source       = { :git => "https://git.oschina.net/baiyingqiu/MyAdditions.git", :tag => "#{s.version}" }
  s.source_files = "MyAdditions/*.{h,m}"
  s.requires_arc = true
  s.platform     = :ios, "7.0"
  # s.frameworks   = "UIKit", "Foundation"

  # User
  s.author             = { "BY" => "qiubaiyingios@163.com" }
  s.social_media_url   = "http://qiubaiying.github.io"

end
